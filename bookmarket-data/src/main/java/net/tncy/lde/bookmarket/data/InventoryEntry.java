package net.tncy.lde.bookmarket.data;

import java.util.ArrayList;

public class InventoryEntry {
    private Book book;
    private Bookstore bookstore;
    private Integer quantity;

    public InventoryEntry(String bookName, String storeName, Integer quantity) {
        ArrayList<Book> books = book.getBooks();
        ArrayList<Bookstore> bookstores = bookstore.getBookstores();

        for (Book book : books) {
            if (book.getName() == bookName) {
                this.book = book;
            }
        }

        for (Bookstore bookstore : bookstores) {
            if (bookstore.getStoreName() == storeName) {
                this.bookstore = bookstore;
            }
        }

        this.quantity = quantity;
    }

    public Book getBook() { return this.book; }
    public Bookstore getBookstore() { return this.bookstore; }
    public Integer getQuantity() { return this.quantity; }
}
