package net.tncy.lde.bookmarket.data;

import java.util.ArrayList;

public class Bookstore {
    private ArrayList<Bookstore> bookstores = new ArrayList<>();
    private String storeName;

    public Bookstore(String storeName) {
        this.storeName = storeName;

        this.bookstores.add(this);
    }

    public ArrayList<Bookstore> getBookstores() { return this.bookstores; }
    public String getStoreName() {
        return this.storeName;
    }
}