package net.tncy.lde.bookmarket.data;

import net.tncy.lde.validator.ISBN;

import java.util.ArrayList;

public class Book {
    private ArrayList<Book> books = new ArrayList<>();

    private String name;
    private String author;

    @ISBN
    private ISBN isbn;

    public Book(String name, String author) {
        this.name = name;
        this.author = author;

        this.books.add(this);
    }

    public ArrayList<Book> getBooks() { return this.books; }
    public String getName() {
        return this.name;
    }
    public String getAuthor() {
        return this.author;
    }
}